#!/bin/bash

pip install robotframework==5.0
pip install robotframework-seleniumlibrary==6.0
wget -q https://github.com/mozilla/geckodriver/releases/download/v0.31.0/geckodriver-v0.31.0-linux64.tar.gz
tar -xzf geckodriver-v0.31.0-linux64.tar.gz
mv geckodriver /usr/local/bin/geckodriver
chmod +x /usr/local/bin/geckodriver
rm geckodriver-v0.31.0-linux64.tar.gz
