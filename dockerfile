FROM kasmweb/firefox:1.10.0-rolling

USER root

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt update

RUN apt install -y python3.10
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 2
RUN python3 --version

RUN apt remove -y --purge python3-apt
RUN apt autoclean
RUN apt install python3-apt
RUN apt install -y python3.10-distutils
RUN apt install curl
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN python3.10 get-pip.py
RUN pip3 --version
RUN pip3 install robotframework==5.0
RUN pip3 install robotframework-seleniumlibrary==6.0.0
RUN apt-get install -y openjdk-11-jre 
RUN apt-get install -y unzip 

# chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt -y install ./google-chrome-stable_current_amd64.deb

# Install drivers
RUN wget -q https://github.com/mozilla/geckodriver/releases/download/v0.31.0/geckodriver-v0.31.0-linux64.tar.gz
RUN tar -C /usr/bin/ -xf geckodriver-*.tar.gz
RUN rm -rf /geckodriver-*.tar.gz

RUN wget https://chromedriver.storage.googleapis.com/101.0.4951.41/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN mv chromedriver /usr/bin/chromedriver 
RUN chown root:root /usr/bin/chromedriver
RUN chmod +x /usr/bin/chromedriver 

RUN useradd -ms /bin/bash  user

USER user
WORKDIR /home/user

ADD . /robot-tests
